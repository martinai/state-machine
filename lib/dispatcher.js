/**
 * This class takes in a message and a user's state, decides the appropriate state, runs it
 * @author Jaime Rump
 */
const _ = require('lodash');
const { matches } = require('./matcher');

class Dispatcher {

  /**
   * Creates a dispatcher
   * @param  {Array<Transition>} transitions
   * @param  {Array<Action>} actions 
   * @return {Dispatcher} 
   */
  constructor(transitions, actions) {
    this.transitions = transitions;
    this.actions = actions;
  }

  /**
   * Picks out the first transition that matches the user's state and the message
   * @param  {Message} message   
   * @param  {Object} user_state 
   * @return {Transition} 
   */
  findMatchingTransition(message, user_state) {

    return _.find( this.transitions, (candidate) => {
      return matches(candidate.match, message, user_state);
    });

  }  

  /**
   * Executes the specified transition on the given message and state
   * @param  {Transition} transition
   * @param  {Message} message 
   * @param  {Object} user_state
   * @return {ResponseChain} 
   */
  async executeTransition(transition, message, user_state) {
    return await this.actions[ transition.action ]( message, user_state );
  } 

  /**
   * Finds a transition that matches the given message and state, runs it, and returns 
   * the response chain
   * @param  {Message} message   
   * @param  {Object} user_state 
   * @return {ResponseChain} 
   */
  async processMessage(message, user_state) {

    let transition = this.findMatchingTransition(message, user_state);

    if( _.isEmpty(transition) ) {
      // Handle error somehow
      throw new Error("No matching transition");
    } else {
      return await this.executeTransition(transition, message, user_state);
    }

  }

}

module.exports = Dispatcher;