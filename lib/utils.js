const _ = require('lodash');

/**
 * Gets the given piece of data out of the parent object
 *
 * getDatum('a', { a: 'a' }) => 'a'
 * getDatum('a.b', { a: 'a' }) => null
 * getDatum('a.b', { a: { b: 'b'} }) => 'b'
 * getDatum('a.b', { a: [{ b: 'b'}, { b: 'c' }] }) => ['b', 'c']
 * getDatum('a.b', { a: { b: ['b', 'c'] } }) => ['b', 'c']
 * getDatum('a.b.c', { a: { b: { c: 'd' } } }) => 'd'
 * getDatum('a.c', { a: [{ b: 'b'}, { b: 'c' }] }) => [null, null]
 * 
 * @param {String} path
 * @param {Object | Array} parent The object to pull the path out of
 * @return {Any}
 */
const getDatum = (path, parent) => {

  let keys = path.split('.');
  let path_segment = keys.shift();
  let new_path = keys.join('.');

  if( _.isEmpty(parent) ) {
    // Parent is empty
    return null;
  } else if( _.isArray( parent ) ) {
    // Pull the key out of each
    return _.map( parent, (child) => {
      return getDatum( path, child );
    });
  } else if( _.isEmpty(keys) ) {
    // No more keys
    return (parent[ path_segment ] == undefined) ? null : parent[ path_segment ];
  }  else if( _.isEmpty( parent[ path_segment ] ) ) {
    // Nothing to find
    return null;
  } else {
    // Keep digging
    return getDatum( new_path, parent[ path_segment ] );
  }

}

/**
 * Returns the result of a comparison
 * If the datum is an array, 'eq' functions as 'includes' and 'ne' functions as '!includes'
 * @param  {Any} datum 
 * @param  {String} comparison A string representation of the comparison to be performed ['eq', 'ne', 'gt', 'gte', 'lt', 'lte']
 * @param  {Any} value 
 * @return {Boolean}  
 */
const compare = (datum, comparison, value) => {

  switch(comparison) {
    case 'eq':

      if( _.isArray(datum) ) {
        // Is an array, check if value is included
        return _.includes(datum, value);
      } else if ( isNumeric(datum) ) {
        // Is numeric, perform float comparison
        return parseFloat(datum) == parseFloat(value);
      } else {
        return _.isEqual(datum, value);
      }

    case 'ne':

      if( _.isArray(datum) ) {
        // Is an array, check if value is included
        return !_.includes(datum, value);
      } else if ( isNumeric(datum) ) {
        // Is numeric, perform float comparison
        return parseFloat(datum) != parseFloat(value);
      } else {
        return !_.isEqual(datum, value);
      }

    case 'gt':

      if( isNumeric(datum) ) {
        return parseFloat(datum) > parseFloat(value);
      } else {
        // Invalid comparison
        return false;
      }

    case 'gte':

      if( isNumeric(datum) ) {
        return parseFloat(datum) >= parseFloat(value);
      } else {
        // Invalid comparison
        return false;
      }

    case 'lt':

      if( isNumeric(datum) ) {
        return parseFloat(datum) < parseFloat(value);
      } else {
        // Invalid comparison
        return false;
      }

    case 'lte':

      if( isNumeric(datum) ) {
        return parseFloat(datum) <= parseFloat(value);
      } else {
        // Invalid comparison
        return false;
      }

    default:
      return false;
  }

}

// Whether a piece of data is numeric, including numeric strings
const isNumeric = (candidate) => {
  return typeof candidate != 'boolean' && !isNaN(candidate);
}

module.exports = {
  getDatum,
  compare
};