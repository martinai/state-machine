/**
 * Checks whether a given transition matches a wide variety of criteria
 * @author Jaime Rump
 */

const _ = require('lodash');
const {
  getDatum,
  compare
} = require('./utils');

/**
 * Decides whether the given transition matches the given message and state
 * @param  {Transition} transition
 * @param  {Message} message 
 * @param  {State} user_state
 * @return {Boolean}           
 */
const matches = (match_criteria, message, user_state) => {

  let conditions = [];

  // Check text
  if( match_criteria.text ) {
    conditions.push( matchesText(match_criteria.text, message, user_state) );
  }

  // Check machine state
  if( match_criteria.from ) {
    conditions.push( matchesMachineState(match_criteria.from, message, user_state) );
  }

  // Check user state
  if( match_criteria.state ) {
    conditions.push( matchesUserState(match_criteria.state, message, user_state) );
  }

  // Check attachments
  if( match_criteria.attachment ) {
    conditions.push( matchesAttachment(match_criteria.attachment, message, user_state) );
  }

  // Check nlp
  if( match_criteria.intent || match_criteria.entities ) {
    conditions.push( matchesNLP(match_criteria, message, user_state) );
  }

  // Return true only if all are true
  return _.find( conditions, (condition) => { return !condition } ) == undefined;

}

/**
 * Decides whether the given transition matches the given message
 * @param  {String | Array} match_criteria
 * @param  {Message} message 
 * @return {Boolean}
 */
const matchesText = (match_criteria, message) => {

  if( _.isArray(match_criteria) ) {

    // Multiple options, true if any of them match
    return !_.isEmpty( _.find( match_criteria, (candidate) => { return matchesText(candidate, message) } ) );

  } else if( _.endsWith(match_criteria, '*') ) {

    // Searching for dynamic text
    let search_string = _.trimEnd(match_criteria, '*');
    return _.startsWith(message.text, search_string);

  } else {

    // Looking for exact match
    return message.text == match_criteria;

  }

}

/**
 * Decides whether the given transition matches the machine state
 * @param  {String | Array} match_criteria
 * @param  {Message} message 
 * @param  {State} user_state
 * @return {Boolean}
 */
const matchesMachineState = (match_criteria, message, user_state) => {

  if( _.isArray(match_criteria) ) {

    // Multiple options, true if any of them match
    return !_.isEmpty( _.find( match_criteria, (candidate) => { return matchesMachineState(candidate, message, user_state) } ) );

  } else if( _.trim(match_criteria) == '*' ) {

    // Wildcard, anything will match
    return true;

  } else {

    // Looking for a specific state
    return match_criteria == (user_state.current_state || user_state.state);

  }

}

/**
 * Decides whether the given transition matches the user state
 * @param  {Object | Array} match_criteria
 * @param  {Message} message 
 * @param  {State} user_state
 * @return {Boolean}           
 */
const matchesUserState = (match_criteria, message, user_state) => {

  if( _.isArray( match_criteria ) ) {
    // An array of state comparisons
    // Return true only if they all match = return false if any doesn't match
    return _.find( match_criteria, (criterion) => { return !matchesUserState(criterion, message, user_state) } ) == undefined;
  } else {

    // Single comparison
    let comparison_datum = getDatum( match_criteria.key, user_state );
    return compare( comparison_datum, match_criteria.comparison, match_criteria.value);

  }

}

/**
 * Decides whether the given transition matches the given NLP data
 * @param  {Object} match_criteria
 * @param  {Message} message 
 * @param  {State} user_state
 * @return {Boolean}           
 */
const matchesNLP = (match_criteria, message, user_state) => {

  if( !message.nlp ) {
    return false;
  }

  // If no intent, it counts as true
  let intent_matches = ( match_criteria.intent ) ? match_criteria.intent == message.nlp.intent : true;

  // Get values of all entities
  let entity_keys = _.keys(match_criteria.entities);
  let entity_matches = _.map( entity_keys, (key) => {

    if( match_criteria.entities[key].presence ) {
      // Just checking if the value is there
      return !_.isEmpty( message.nlp.entities[key] );
    } else if( match_criteria.entities[key].presence == false ) {
      // Checking for its absense
      return !_.includes( _.keys(message.nlp.entities, key) );
    } else {
      // Checking if any of the values match
      return _.find(message.nlp.entities[key], (entity_values) => {
        return _.isEqual(entity_values.value, match_criteria.entities[key].value);
      }) != undefined;
    }

  });

  // Check if any don't match
  let entities_all_match = ( _.isEmpty(entity_keys) ) || _.find(entity_matches, (candidate) => { return !candidate }) == undefined;

  return intent_matches && entities_all_match;

}

/**
 * Decides whether the given transition matches the given attachment data
 * @param  {Object} match_criteria
 * @param  {Message} message 
 * @return {Boolean}           
 */
const matchesAttachment = (match_criteria, message) => {

  if( match_criteria.type ) {
    // Returns true if any attachments are of the right type
    return _.find( message.attachments, (attachment) => { return attachment.type == match_criteria.type }) != undefined;
  } else if( match_criteria.presence === false ) {
    // Specifically looking for no files
    return _.isEmpty(message.attachments);
  } else {
    // Simply looking to see if files are there
    return !_.isEmpty(message.attachments);
  }

}

module.exports = {
  matches,
  matchesText,
  matchesMachineState,
  matchesUserState,
  matchesNLP,
  matchesAttachment
};