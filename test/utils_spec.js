const {
  getDatum,
  compare
} = require('../lib/utils');
const assert = require('assert');

describe("Utils", () => {

  describe(".getDatum", () => {

    it("digs out surface level keys", () => {

      let object = { a: 'a' };
      assert.equal( getDatum('a', object), 'a' );

    }); // it("digs out surface level keys")

    it("returns null for undefined keys", () => {

      let object = { a: 'a' };
      assert.equal( getDatum('b', object), null );

    }); // it("returns null for undefined keys")

    it("preserves false values", () => {

      let object = { a: false };
      assert(!getDatum('a', object));

    }); // it("preserves false values")

    it("digs out nested keys", () => {

      let object = { a: { b: 'b' } };
      assert.equal( getDatum('a.b', object), 'b' );

    }); // it("digs out nested keys")

    it("digs through arrays", () => {

      let object = { a: [{ b: 'b' }, { b: 'c' }] };
      assert.deepEqual( getDatum('a.b', object), ['b', 'c'] );      

    }); // it("digs through arrays")

    it("can return arrays of nulls", () => {

      let object = { a: [{ b: 'b' }, { b: 'c' }] };
      assert.deepEqual( getDatum('a.c', object), [null, null] );  

    }); // it("can return arrays of nulls")

  }); // describe(".getDatum")

  describe(".compare", () => {

    context("eq", () => {

      it("compares floats if datum is numeric", () => {

        let datum = "12.00";
        let comparison = "eq";
        let value = 12;

        assert(compare(datum, comparison, value));

      }); // it("compares floats if datum is numeric")

      it("compares using deep equality", () => {

        let datum = { a: 'a' };
        let comparison = "eq";
        let value = { a: 'a' };

        assert(compare(datum, comparison, value));

      }); // it("compares using deep equality")

      it("returns false for bad comparisons", () => {

        let datum = { a: 'a' };
        let comparison = "eq";
        let value = { a: 'b' };

        assert(!compare(datum, comparison, value));

      }); // it("returns false for bad comparisons")

      it("is an include if datum is array", () => {

        let datum = ['a', 'b', 'c'];
        let comparison = "eq";
        let value = 'b';

        assert(compare(datum, comparison, value));

      }); // it("is an include if datum is array")

    }); // context("eq")

    context("ne", () => {

      it("compares floats if datum is numeric", () => {

        let datum = "12.00";
        let comparison = "ne";
        let value = 12;

        assert(!compare(datum, comparison, value));

      }); // it("compares floats if datum is numeric")

      it("compares using deep equality", () => {

        let datum = { a: 'a' };
        let comparison = "ne";
        let value = { a: 'a' };

        assert(!compare(datum, comparison, value));

      }); // it("compares using deep equality")

      it("means not-includes if datum is array", () => {

        let datum = ['a', 'b', 'c'];
        let comparison = "ne";
        let value = 'b';

        assert(!compare(datum, comparison, value));

      }); // it("means not-includes if datum is array")

    }); // context("ne")

    context("gt", () => {

      it("returns false for anything non-numeric", () => {

        let datum = { a: 'a' };
        let comparison = "gt";
        let value = 12;

        assert(!compare(datum, comparison, value));

      }); // it("returns false for anything non-numeric")

      it("works as expected for numeric values", () => {

        let datum = "12";
        let comparison = "gt";
        let value = 11;

        assert(compare(datum, comparison, value));

        datum = 9;
        assert(!compare(datum, comparison, value));

      }); // it("works as expected for numeric values")

    }); // context("gt")

    context("gte", () => {

      it("returns false for anything non-numeric", () => {

        let datum = { a: 'a' };
        let comparison = "gte";
        let value = 12;

        assert(!compare(datum, comparison, value));

      }); // it("returns false for anything non-numeric")

      it("works as expected for numeric values", () => {

        let datum = "12";
        let comparison = "gte";
        let value = 11;

        assert(compare(datum, comparison, value));

        datum = 11;
        assert(compare(datum, comparison, value));

        datum = 9;
        assert(!compare(datum, comparison, value));

      }); // it("works as expected for numeric values")

    }); // context("gte")

    context("lt", () => {

      it("returns false for anything non-numeric", () => {

        let datum = { a: 'a' };
        let comparison = "lt";
        let value = 12;

        assert(!compare(datum, comparison, value));

      }); // it("returns false for anything non-numeric")

      it("works as expected for numeric values", () => {

        let datum = "11";
        let comparison = "lt";
        let value = 12;

        assert(compare(datum, comparison, value));

        datum = 13;
        assert(!compare(datum, comparison, value));

      }); // it("works as expected for numeric values")

    }); // context("lt")

    context("lte", () => {

      it("returns false for anything non-numeric", () => {

        let datum = { a: 'a' };
        let comparison = "lte";
        let value = 12;

        assert(!compare(datum, comparison, value));

      }); // it("returns false for anything non-numeric")

      it("works as expected for numeric values", () => {

        let datum = "11";
        let comparison = "lte";
        let value = 12;

        assert(compare(datum, comparison, value));

        datum = 12;
        assert(compare(datum, comparison, value));

        datum = 13;
        assert(!compare(datum, comparison, value));

      }); // it("works as expected for numeric values")

    }); // context("lte")

  }); // describe(".compare")

}); // describe("Utils")