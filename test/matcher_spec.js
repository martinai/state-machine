const {
  matches,
  matchesText,
  matchesMachineState,
  matchesUserState,
  matchesNLP,
  matchesAttachment
} = require('../lib/matcher');
const assert = require('assert');

describe('Matcher', () => {

  describe('.matches', () => {

    it("can match based on text", () => {

      let match_criteria = {
        text: "exact_text"
      };
      let message = {
        text: "exact_text"
      };
      let user_state = {
      };

      assert(matches(match_criteria, message, user_state));

    }); // it("can match based on text")

    it("can match based on user state", () => {

      let match_criteria = {
        state: {
          key: "not_this_count",
          comparison: "gte",
          value: 5
        }
      };
      let message = {
        text: "exact_text"
      };
      let user_state = {
        not_this_count: 6
      };

      assert(matches(match_criteria, message, user_state));

    }); // it("can match based on user state")

    it("can match based on machine state", () => {

      let match_criteria = {
        from: 'origin'
      };
      let message = {
        text: "exact_text"
      };
      let user_state = {
        current_state: 'origin'
      };

      assert(matches(match_criteria, message, user_state));

    }); // it("can match based on machine state")

    it("can match based on NLP", () => {

      let match_criteria = {
        intent: "test_match",
        entities: {
          match: {
            presence: true
          }
        }
      };
      let message = {
        text: "exact_text",
        nlp: {
          intent: "test_match",
          entities: {
            match: [
              {
                value: true
              }
            ]
          }
        }
      };
      let user_state = {
      };

      assert(matches(match_criteria, message, user_state));

    }); // it("can match based on NLP")

    it("can match based on attachments", () => {

      let match_criteria = {
        attachment: {
          type: 'image'
        }
      };
      let message = {
        text: "exact_text",
        attachments: [
          {
            type: 'image'
          }
        ]
      };
      let user_state = {
      };

      assert(matches(match_criteria, message, user_state));

    }); // it("can match based on attachments")

    it("returns false if any condition is false", () => {

      let match_criteria = {
        text: "exact_text",
        state: {
          key: "not_this_count",
          comparison: "gte",
          value: 5
        }
      };
      let message = {
        text: "exact_text"
      };
      let user_state = {
        not_this_count: 4
      };

      assert(!matches(match_criteria, message, user_state));

    }); // it("returns false if any condition is false")

    it("returns true if all conditions are true", () => {

      let match_criteria = {
        text: "exact_text",
        state: {
          key: "not_this_count",
          comparison: "gte",
          value: 5
        }
      };
      let message = {
        text: "exact_text"
      };
      let user_state = {
        not_this_count: 6
      };

      assert(matches(match_criteria, message, user_state));

    }); // it("returns true if all conditions are true")

    context("with no criteria", () => {

      it("will always match", () => {

        let match_criteria = {
        };
        let message = {
          text: "exact_text"
        };
        let user_state = {
          not_this_count: 6
        };

        assert(matches(match_criteria, message, user_state));

      }); // it("will always match")

    }); // context("with no criteria")

  }); // describe('.matches')

  describe('.matchesText', () => {

    // Exact text

    it("matches exact text", () => {

      let match_criteria = {
        text: "exact text"
      };
      let message = {
        text: "exact text"
      };

      assert(matchesText(match_criteria.text, message));

    }); // it("matches exact text")

    it("doesn't match inexact text", () => {

      let match_criteria = {
        text: "exact text"
      };
      let message = {
        text: "inexact text"
      };

      assert(!matchesText(match_criteria.text, message));

    }); // it("doesn't match inexact text")

    // Wildcards
    
    it("matches wildcards at the end", () => {

      let match_criteria = {
        text: "match*"
      };
      let message = {
        text: "match wildcard"
      };

      assert(matchesText(match_criteria.text, message));

    }); // it("matches wildcards at the end")

    it("matches pure wildcards", () => {

      let match_criteria = {
        text: "*"
      };
      let message = {
        text: "match wildcard"
      };

      assert(matchesText(match_criteria.text, message));

    }); // it("matches pure wildcards")

    it("doesn't match wildcards in middle", () => {

      let match_criteria = {
        text: "wild * card"
      };
      let message = {
        text: "wild and crazy card"
      };

      assert(!matchesText(match_criteria.text, message));

    }); // it("doesn't match wildcards in middle")

    // Arrays
    
    it("matches multiple options", () => {

      let match_criteria = {
        text: ["exact text", "other option"]
      };
      let message = {
        text: "exact text"
      };

      assert(matchesText(match_criteria.text, message));

    }); // it("matches multiple options")

    it("only matches if at least one option matches", () => {

      let match_criteria = {
        text: ["exact text", "other option"]
      };
      let message = {
        text: "third option"
      };

      assert(!matchesText(match_criteria.text, message));

    }); // it("only matches if at least one option matches")

    it("matches wildcards in arrays", () => {

      let match_criteria = {
        text: ["match*", "other option"]
      };
      let message = {
        text: "match wildcard"
      };

      assert(matchesText(match_criteria.text, message));

    }); // it("matches wildcards in arrays")

  }); // describe('.matchesText')

  describe('.matchesMachineState', () => {

    it("matches exact state", () => {

      let match_criteria = {
        from: "state"
      };
      let user_state = {
        current_state: "state"
      };

      assert(matchesMachineState(match_criteria.from, {}, user_state));

    }); // it("matches exact state")

    it("doesn't match inexact state", () => {

      let match_criteria = {
        from: "exact_state"
      };
      let user_state = {
        current_state: "inexact_state"
      };

      assert(!matchesMachineState(match_criteria.from, {}, user_state));

    }); // it("doesn't match inexact state")

    it("always matches wildcards", () => {

      let match_criteria = {
        from: "*"
      };
      let user_state = {
        current_state: Math.random().toString(36)
      };

      assert(matchesMachineState(match_criteria.from, {}, user_state));

    }); // it("always matches wildcards")

    it("matches user_state.current_state or user_state.state", () => {

      let match_criteria = {
        from: "state"
      };
      let user_state = {
        state: "state"
      };

      assert(matchesMachineState(match_criteria.from, {}, user_state));

    }); // it("matches user_state.current_state or user_state.state")

    it("Prefers current_state over state", () => {

      let match_criteria = {
        from: "state"
      };
      let user_state = {
        current_state: "state",
        state: "something_else"
      };

      assert(matchesMachineState(match_criteria.from, {}, user_state));

    }); // it("Prefers current_state over state")

    // Arrays

    it("matches multiple options", () => {

      let match_criteria = {
        from: ["exact state", "other option"]
      };
      let user_state = {
        current_state: "exact state"
      };

      assert(matchesMachineState(match_criteria.from, {}, user_state));

    }); // it("matches multiple options")

    it("only matches if at least one option matches", () => {

      let match_criteria = {
        from: ["exact state", "other option"]
      };
      let user_state = {
        current_state: "third_option"
      };

      assert(!matchesMachineState(match_criteria.from, {}, user_state));

    }); // it("only matches if at least one option matches")

  }); // describe('.matchesMachineState')

  describe('.matchesUserState', () => {

    it("performs the comparison", () => {

      let match_criteria = {
        state: {
          key: "authenticated",
          comparison: "eq",
          value: true
        }
      };
      let user_state = {
        authenticated: true
      };

      assert(matchesUserState(match_criteria.state, {}, user_state));

    }); // it("performs the comparison")

    context("when given an array", () => {

      it("is true if they are all true", () => {

        let match_criteria = {
          state: [
            {
              key: "not_this_count",
              comparison: "gte",
              value: 5
            },
            {
              key: "authenticated",
              comparison: "eq",
              value: true
            }
          ]
        };
        let user_state = {
          not_this_count: 5,
          authenticated: true
        };

        assert(matchesUserState(match_criteria.state, {}, user_state));        

      }); // it("is true if they are all true")

      it("is false if any are false", () => {

        let match_criteria = {
          state: [
            {
              key: "not_this_count",
              comparison: "gte",
              value: 5
            },
            {
              key: "authenticated",
              comparison: "eq",
              value: true
            }
          ]
        };
        let user_state = {
          not_this_count: 5,
          authenticated: false
        };

        assert(!matchesUserState(match_criteria.state, {}, user_state));        

      }); // it("is false if any are false")

    }); // context("when given an array")

  }); // describe('.matchesUserState')

  describe('.matchesNLP', () => {

    it("returns false if the message has no NLP", () => {

      let match_criteria = {
        intent: 'test_intent'
      };
      let message = {
      };

      assert(!matchesNLP(match_criteria, message));  

    }); // it("returns false if the message has no NLP")

    it("can match an intent", () => {

      let match_criteria = {
        intent: 'test_intent'
      };
      let message = {
        nlp: {
          intent: 'test_intent'
        }
      };

      assert(matchesNLP(match_criteria, message));  

    }); // it("can match an intent")

    it("can match an intent and entities", () => {

      let match_criteria = {
        intent: 'test_intent',
        entities: {
          datetime: {
            presence: true
          }
        }
      };
      let message = {
        nlp: {
          intent: 'test_intent',
          entities: {
            datetime: [
              {
                value: '2017-01-01'
              }
            ]
          }
        }
      };

      assert(matchesNLP(match_criteria, message));  

    }); // it("can match an intent and entities")

    it("can match the value of an entity", () => {

      let match_criteria = {
        intent: 'test_intent',
        entities: {
          datetime: {
            value: '2017-01-02'
          }
        }
      };
      let message = {
        nlp: {
          intent: 'test_intent',
          entities: {
            datetime: [
              {
                value: '2017-01-01'
              },
              {
                value: '2017-01-02'
              }
            ]
          }
        }
      };

      assert(matchesNLP(match_criteria, message));  

    }); // it("can match the value of an entity")

    it("can match the absence of an entity", () => {

      let match_criteria = {
        intent: 'test_intent',
        entities: {
          datetime: {
            presence: false
          }
        }
      };
      let message = {
        nlp: {
          intent: 'test_intent',
          entities: {
          }
        }
      };

      assert(matchesNLP(match_criteria, message)); 

    }); // it("can match the absence of an entity")

    it("matches only if intent and entities both match", () => {

      let match_criteria = {
        intent: 'test_intent',
        entities: {
          datetime: {
            presence: true
          }
        }
      };
      let message = {
        nlp: {
          entities: {
            datetime: [
              {
                value: '2017-01-01'
              }
            ]
          }
        }
      };

      assert(!matchesNLP(match_criteria, message));

    }); // it("matches only if intent and entities both match")

    it("doesn't match if any entities don't match", () => {

      let match_criteria = {
        entities: {
          datetime: {
            presence: true
          },
          other: {
            presence: true
          }
        }
      };
      let message = {
        nlp: {
          entities: {
            datetime: [
              {
                value: '2017-01-01'
              }
            ]
          }
        }
      };

      assert(!matchesNLP(match_criteria, message));

    }); // it("doesn't match if any entities don't match")

    it("matches if all entities match", () => {

      let match_criteria = {
        entities: {
          datetime: {
            presence: true
          },
          other: {
            presence: true
          }
        }
      };
      let message = {
        nlp: {
          entities: {
            datetime: [
              {
                value: '2017-01-01'
              }
            ],
            other: {
              value: true
            }
          }
        }
      };

      assert(matchesNLP(match_criteria, message));

    }); // it("matches if all entities match")

  }); // describe('.matchesNLP')

  describe('.matchesAttachment', () => {

    it("matches if there are any attachments of the right type", () => {

      let match_criteria = {
        attachment: {
          type: "image"
        }
      };
      let message = {
        attachments: [
          {
            type: "video"
          },
          {
            type: "image"
          }
        ]
      };

      assert(matchesAttachment(match_criteria.attachment, message));

    }); // it("matches if there are any attachments of the right type")

    it("doesn't match if there are no attachments of the right type", () => {

      let match_criteria = {
        attachment: {
          type: "video"
        }
      };
      let message = {
        attachments: [
          {
            type: "image"
          },
          {
            type: "image"
          }
        ]
      };

      assert(!matchesAttachment(match_criteria.attachment, message));

    }); // it("doesn't match if there are no attachments of the right type")

    it("can match based on attachment presence", () => {

      let match_criteria = {
        attachment: {
          presence: true
        }
      };
      let message = {
        attachments: [
          {
            type: "image"
          },
          {
            type: "image"
          }
        ]
      };

      assert(matchesAttachment(match_criteria.attachment, message));

    }); // it("can match based on attachment presence")

    it("presence match fails if no attachments", () => {

      let match_criteria = {
        attachment: {
          presence: true
        }
      };
      let message = {
        attachments: [
        ]
      };

      assert(!matchesAttachment(match_criteria.attachment, message));

    }); // it("presence match fails if no attachments")

    it("can match based on attachment absence", () => {

      let match_criteria = {
        attachment: {
          presence: false
        }
      };
      let message = {
        attachments: [
        ]
      };

      assert(matchesAttachment(match_criteria.attachment, message));

    }); // it("can match based on attachment absence")

    it("absence match fails when attachments are present", () => {

      let match_criteria = {
        attachment: {
          presence: false
        }
      };
      let message = {
        attachments: [
          {
            type: "image"
          },
          {
            type: "image"
          }
        ]
      };

      assert(!matchesAttachment(match_criteria.attachment, message));

    }); // it("absence match fails when attachments are present")

  }); // describe('.matchesAttachment')

}); // describe('Matcher')