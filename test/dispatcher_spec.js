const assert = require('assert');
const Dispatcher = require('../lib/dispatcher');

describe("Dispatcher", () => {

  describe("#findMatchingTransition", () => {

    it("picks the first matching transition", () => {

      // Dispatcher params
      let transitions = [
        {
          name: "first_transition",
          match: {
            text: "nonmatch_text"
          },
          action: "first_action"
        },
        {
          name: "second_transition",
          match: {
            text: "match_text"
          },
          action: "second_action"
        },
        {
          name: "duplicate_transition",
          match: {
            text: "match_text"
          },
          action: "duplicate_action"
        }
      ];
      let actions = {
        first_action: function() {},
        second_action: function() {},
        duplicate_action: function() {}
      };
      let dispatcher = new Dispatcher(transitions, actions);

      // Matcher params
      let message = {
        text: "match_text"
      };

      // Should have picked first
      assert.equal( dispatcher.findMatchingTransition(message, {}), transitions[1] );

    }); // it("picks the first matching transition")

    it("returns undefined if no match", () => {

      // Dispatcher params
      let transitions = [
      ];
      let actions = {
        first_action: function() {}
      };
      let dispatcher = new Dispatcher(transitions, actions);

      // Matcher params
      let message = {
        text: "match_text"
      };

      // Should have picked first
      assert.equal( dispatcher.findMatchingTransition(message, {}), undefined );

    }); // if("returns undefined if no match")

  }); // describe("#findMatchingTransition")

  describe("#executeTransition", () => {

    it("rethrows any errors", (done) => {

      // Dispatcher params
      let transitions = [
        {
          match: {},
          action: 'first_action'
        }
      ];
      let actions = {
        first_action: function() {
          throw new Error("Testing");
        }
      };
      let dispatcher = new Dispatcher(transitions, actions);

      // Matcher params
      let message = {
        text: "match_text"
      };

      dispatcher.executeTransition(transitions[0], message, {}).catch((err) => {
        assert.equal(err.message, "Testing");
        done();
      });

    }); // it("rethrows any errors")

    it("returns the results of the action", async () => {

      // Dispatcher params
      let transitions = [
        {
          match: {},
          action: 'first_action'
        }
      ];
      let actions = {
        first_action: function( message, user_state) {
          return message;
        }
      };
      let dispatcher = new Dispatcher(transitions, actions);

      // Matcher params
      let message = {
        text: "match_text"
      };

      let result = await dispatcher.executeTransition(transitions[0], message, {});
      assert.equal( result, actions.first_action(message, {}) );

    }); // it("returns the results of the action")

  }); // describe("#executeTransition")

  describe("#processMessage", () => {

    it("throws error if no matching state", (done) => {

      // Dispatcher params
      let transitions = [
      ];
      let actions = {
        first_action: function() {}
      };
      let dispatcher = new Dispatcher(transitions, actions);

      // Matcher params
      let message = {
        text: "match_text"
      };

      dispatcher.processMessage(message, {}).catch((err) => {
        assert.equal(err.message, "No matching transition");
        done();
      });

    }); // it("throws error if no matching state")

    it("returns the results of the action", async () => {

      // Dispatcher params
      let transitions = [
        {
          match: {},
          action: 'first_action'
        }
      ];
      let actions = {
        first_action: function( message, user_state) {
          return message;
        }
      };
      let dispatcher = new Dispatcher(transitions, actions);

      // Matcher params
      let message = {
        text: "match_text"
      };

      let result = await dispatcher.processMessage(message, {});
      assert.equal( result, actions.first_action(message, {}) );

    }); // it("returns the results of the action")

  }); // describe("#processMessage")

}); // describe("Dispatcher")