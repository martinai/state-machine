# State-Machine
State-Machine is an extremely flexible transition-first state machine framework. What that means is that it's a skeleton upon which to build state machines that care more about what happens on a state change rather than knowing what the current state is.

## Usage
```
let StateMachine = require("state-machine");
let state_machine = new StateMachine( transitions, actions );
let response_chain = await state_machine.processMessage( message, user_state );
```

## Transitions
The core of State-machine is transitions, its purpose is to figure out which state to transition to and what to do when changing state. A transition looks like:
```
{
  name: "Example Transition",
  description: "Optional description for your convenience",
  match: {
    text: "I am a transition"
  },
  action: "function_to_run_on_transition"
}
```
When a message is received, it is matched to a transition, then that transition's action is run on the message.

## Actions
Actions are what happens when a message is received. Actions are function receive a message and the user's state. The user's state is an object with a `sender_id` and `current_state`. The message is an object like:
```
{
  text: "Blah",
  nlp: {
    intent: "Example_message",
    entities: {
      datetime: [
        {
          value: "2017-01-01"
        }
      ]
    }
  },
  attachments: [
    {
      type: "image",
      url: "file.jpg"
    }
  ]
}
```
An action performs any state changes or other actions and returns a chain of responses like: 
```
[
  {
    message: {
      text: "Is this a response?",
      quick_replies: [ "yes", "no" ]
    }
  },
  {
    pause: {
      duration: 1000
    }
  },
  {
    template: {
      type: "button",
      payload: {
        text: "An example of a button payload",
        buttons: [
          {
            type: "postback",
            title: "Postback button",
            payload: "example_postback"
          },
          {
            type: "web_url",
            title: "Link button",
            url: "www.google.com"
          }
        ]
      }
    }
  }
]
```
These responses are then sent to the user.

## Match options
State-Machine can match on a wide variety of criteria.

### Text
You can match based on the exact text of a message, if you're using quick replies or postbacks rather than free typing.

You can match on the exact text of a message...
```
{
  name: "Select Page",
  match: {
    text: "Let's begin"
  }
}
```
You can have multiple triggers for the same state...
```
{
  name: "Suggest Articles",
  match: {
    text: ["Let's go now", "Post another article"]
  }
}
```
You can match on dynamic text (at the end of a string)...
```
{
  name: "Choose Article",
  match: {
    text: "ARTICLE_LINK_*"
  }
}
```

### Machine State
You can match on the state in the state machine, if you want events to happen in a certain sequence.

You can match on a single starting state...
```
{
  name: "Get Started",
  match: {
    from: "origin"
  }
}
```
You can match on several starting states...
```
{
  name: "Get Started",
  match: {
    from: ["origin", "other"]
  }
}
```
You can match from any starting state...
```
{
  name: "Get Started",
  match: {
    from: "*"
  }
}
```

### Attachment
You can match based on the presence and type of attached files.

You can match based on attached files...
```
{
  name: "Submit Mission",
  match: {
    attachment: {
      type: "image"
    }
  }
}
```

### NLP/NLU
If you pass NLP data, you can match based on that

You can match based on the presence (or absence) of an entity...
```
{
  name: "Schedule",
  match: {
    entities: {
      datetime: {
        present: true
      }
    }
  }
}
```
You can match on a specific value of an entity...
```
{
  name: "Wednesday",
  match: {
    entities: {
      datetime: {
        value: "Wednesday"
      }
    }
  }
}
```
You can match on an intent...
```
{
  name: "Choose page",
  match: {
    intent: "Choose page"
  }
}
```

### User state
You can match on values in the user's state...
```
{
  name: "5 wrong",
  match: {
    state: [
      {
        key: "not_this_count",
        comparison: "gte",
        value: 5
      }
    ]
  }
```